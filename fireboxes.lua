local function isempty(s)
  return s == nil or s == ''
end

minetest.register_node("firesafety:beepybox", {
    description = "Emergency button for fires",
    tiles = {
    "fire_safety_redside.png",
    "fire_safety_redside.png",
    "fire_safety_redside.png",
    "fire_safety_redside.png",
    "fire_safety_redside.png",
    "fire_safety_fire_button.png"},
	groups = {dig_immediate=2},
    paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.3125, -0.3125, 0.125, -0.1875, 0.3125, 0.5}, -- NodeBox2
			{-0.1875, -0.25, 0.1875, 0.1875, 0.1875, 0.5}, -- NodeBox3
			{-0.25, 0.1875, 0.125, 0.3125, 0.3125, 0.5}, -- NodeBox4
			{0.1875, -0.3125, 0.125, 0.3125, 0.3125, 0.5}, -- NodeBox5
			{-0.25, -0.3125, 0.125, 0.3125, -0.1875, 0.5}, -- NodeBox6
		}
	},
	
	digilines = {
	    effector = {
	        action = function() end
	    }
	},
    
    on_punch = function(pos, node, puncher, pointed_thing)	
        if puncher:is_player() then
            print(puncher:get_player_name() .. " pounded the alarm at ")
            print(pos)
        end
        
		local setchan = minetest.get_meta(pos):get_string("channel")
		
		print("Sending to " ..  setchan)
		    
        digilines.receptor_send(pos, digilines.rules.default, setchan, "fire") -- TODO: HAHA VERY FUNNY DIGILINES NOW PEOPLE ARE DYING IN FIRES AND I'M GOING TO COURT FOR VIOLATING EU LAW; SEE DB-SI ARGHH
        
        -- TODO: store the name of the puncher in somewhere permanent
    end,
    
    on_construct = function(pos)
        local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,
	
	on_receive_fields = function(pos, _, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end
})

