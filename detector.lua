--[[ Heat detector
If a hot node is detected within 30 metres, sound the alarm!
--]]

local hotNodes = {"fire:basic_flame","default:lava_source", "default:torch" }

local function isempty(s)
  return s == nil or s == ''
end

minetest.register_node("firesafety:heatdetector", {
    description = "Heat/Fire Detector",
    tiles = {"fire_safety_detector.png"},
	groups = {dig_immediate=2},
	paramtype2 = "facedir",
	
	tiles = {
		"fire_detector_off.png",
		"firedetectorside.png",
		"fire_detector_off.png",
		"fire_detector_off.png",
		"fire_detector_off.png",
		"fire_detector_off.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.0625, 0.4375, -0.25, 0.125, 0.5, 0.25}, -- NodeBox1
			{-0.125, 0.4375, -0.1875, 0.1875, 0.5, 0.1875}, -- NodeBox2
			{-0.1875, 0.4375, -0.125, 0.25, 0.5, 0.125}, -- NodeBox3
		}
	},
	
	digilines = {
	    effector = {
	        action = function() end
	    }
	},
    
    on_timer = function(pos)
        local thereAreFires = minetest.find_node_near(pos, 20, hotNodes)
        
        if not isempty(thereAreFires) then
            print("Detector at:")
            print(pos)
        
            print("detected fire at:")
            print(thereAreFires)
            
            local messageToSend = "fire"
			
        else
            local messageToSend = "clear"
        end
        
		local setchan = minetest.get_meta(pos):get_string("channel")
		print("sending to " .. setchan)
        digiline:receptor_send(pos, rules, setchan, messageToSend) -- TODO: Fix this crap and make it ACTUALLY SEND. I'm sick and tired of your silly games, digilines. If you want players to die horrific deaths in fires, please continue doing these DAMN STUPID games. TL;DR: FIX YOUR CRAP, DIGILINES!!!!
        
        local timer = minetest.get_node_timer(pos)
        timer:start(1)
        
        return true
    end,
    
    on_construct = function(pos)
        local timer = minetest.get_node_timer(pos)
        timer:start(1)
        
        local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,
	
	on_receive_fields = function(pos, _, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end
})

minetest.register_craft({
    output = "firesafety:heatdetector 10",
    recipe = {
        {"basic_materials:plastic_sheet","digilines:digiline 4","basic_materials:plastic_sheet"},
        {"default:steel_ingot 2","mesecons:luacontroller","default:steel_ingot 2"},
        {"basic_materials:plastic_sheet","bucket:bucket_water","basic_materials:plastic_sheet"},
    }
})

