--[[ Alarm
Upon reciving the digiline message "fire" will play an obnoxious sound, forcing all players who want to keep their eardrums intact to leave the building
]]


local soundhandle

minetest.register_node("firesafety:alarm",{
    description = "Fire Alarm",
    tiles = {"fire_safety_alarm.png"},
	groups = {dig_immediate=2},
    tiles = {
		"fire_detector_off.png",
		"firedetectorside.png",
		"fire_detector_off.png",
		"fire_detector_off.png",
		"fire_detector_off.png",
		"fire_detector_off.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.0625, 0.4375, -0.25, 0.125, 0.5, 0.25}, -- NodeBox1
			{-0.125, 0.4375, -0.1875, 0.1875, 0.5, 0.1875}, -- NodeBox2
			{-0.1875, 0.4375, -0.125, 0.25, 0.5, 0.125}, -- NodeBox3
		}
	},
	paramtype2 = "facedir",
	
    digilines =
	{
		receptor = {},
		effector = {
			action = function(pos, _, channel, msg)
				local setchan = minetest.get_meta(pos):get_string("channel")
				
				--[[if soundhandle then
                    minetest.sound_stop(soundhandle)
                end]]--
				
                if msg == "fire" and channel == setchan then
                    soundhandle = minetest.sound_play("fire",{
                        gain = 1.5,
                        max_hear_distance = 50, 
                    })
                elseif msg == "test" and channel == setchan then
                    soundhandle = minetest.sound_play("test",{
                        gain = 1.5,
                        max_hear_distance = 50, 
                    })
                elseif msg == "clear" and channel == setchan then -- Stop sounds when given the all-clear because players don't deserve fire nor ear damage
                    if soundhandle then
                        minetest.sound_stop(soundhandle)
                    end
                end
            end
		},
	},

    
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,
	
	on_receive_fields = function(pos, _, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end
})
