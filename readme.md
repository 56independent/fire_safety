## Introduction
**Note** The code is nowhere near a working state. Only fire alarms and signs work as intended.

This mod has been kicking around in my brain ever since i started trying out modding, which was in 2020. I really like this idea, so i've now gotten it down:

## Nodes
Here are the nodes you'll be using for fire prevention.
 
* Admin panel - Allows for testing or fire alarms in the network. Zoning is possible using channels for each zone
* Sign-lights - Signs which light up on digiline signal
* Heat detector - Will detect anything hot or with an open flame and send the fire message. May also have information on the class of fire.
* Fire Boxes - Basically a button to say "argh fire!". Right-clicking reveals a log of who pressed it (in my school, they'd use CCTV to detect offenders, but CCTV isn't really a thing in Minetest
* Fire Sounders - Will make an annoying noise when the `fire` message is sent.
* Extinguisher - Will, given a fire message, send fire repellent everywhere. There are various extinguisher classes which only work on specific materials; wrong combinations might kill.
* Fire sprinkler (only if pipeworks is enabled) - Connected to water or foam from pipeworks, the fire sprinkler will explode and release water given the digilines message. Only use with Class A fires.

**Note** The heat detector and fire boxes will not function because digilines is stupid and won't let me send a damn message. Please use the digistuff node detector if you wish to detect fires. Thank you for your understanding in this time of trouble. 

## Extinguisher-fire interactions
Each extinguisher and fire-type works like this. Extinguishers will only send out their respective extinguisher if the fire type matches the one the extinguisher is rated for.

Without the companion `random-fires` mod, interactions are binary; extinguishers either work or do not work. With `random-fires`, the extinguishers will follow these interactions, including potentially dangerous ones.

| Extinguisher | Comes from | A fires (spicy solids) | B fires (spicy liquids) | C fires (spicy gases) | D fires (spicy metal) | E fires (spicy electronics) |
| --- | --- | --- | --- | --- | --- | --- |
| Water mist | Water buckets | **Works** | EXPLOSION SPREADS FIRE ARGH | 1/10 of the fire goes away and then extinguisher explodes | _No effect on fire_ | No effect on fire and coming within 10 metres will kill you |
| CO2 (within 5 metres panicked suffocation occurs) | Elements mod or baking soda, vinegar, and bag | _No effect on fire_ | **Works** | _No effect on fire_ | _No effect on fire_ | **Works** |
| Foam | Water and fire-safe (blast-resistant in the case of technic) concrete, from clay | **Works** | **Works** | Removes fire from the floor | _No effect on fire_ | **Works** |
| Dry powder | Ground-up concrete | _No effect on fire_ | **Works** | **Works** | **Works** | 1/10 of fire goes away then extinguisher explodes |

## Installation of fire systems
All fire systems must have two basic components:

* Fire detectors
* Fire destroyers

To start with, implement detectors. Have a button or heat detector to find fires and send out warning messages. These are vital because fire destroyers are incapable of detecting fires.

Fire destroyers are what stops a fire from getting worse. Add a sprinkler or extinguisher as needed. Sprinklers ensure constant supply of extinguisher, but extinguishers can contain more types of extinguisher.

Additionally, a fire panel, sounders, and signs may be implemented to warn people of a fire and guide them the correct direction. A fire panel allows for test signals as well, meaning that you can ensure the entirety of the premises have working warnings without activating the destroyers.

