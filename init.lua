-- File for functions, and includes the rest of the mod

rules = {{x=0,  y=0,  z=-1}, -- taken from digilines.default.rules thing
            {x=1,  y=0,  z=0},
            {x=-1, y=0,  z=0},
            {x=0,  y=0,  z=1},
            {x=1,  y=1,  z=0},
            {x=1,  y=-1, z=0},
            {x=-1, y=1,  z=0},
            {x=-1, y=-1, z=0},
            {x=0,  y=1,  z=1},
            {x=0,  y=-1, z=1},
            {x=0,  y=1,  z=-1},
            {x=0,  y=-1, z=-1}}

dofile(minetest.get_modpath("firesafety") .. "/alarm.lua")
dofile(minetest.get_modpath("firesafety") .. "/detector.lua")
dofile(minetest.get_modpath("firesafety") .. "/signs.lua")
dofile(minetest.get_modpath("firesafety") .. "/fireboxes.lua")
