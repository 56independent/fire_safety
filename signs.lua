function registerSign(type)
    function registerLightSign(type, level)
        local groups2
    
        if level == 2 then
           local groups2 = {oddly_breakable_by_hand=1, attached_node=1}
        else
           local groups2 = {oddly_breakable_by_hand=1, attached_node=1, not_in_creative_inventory=1}
        end
    
        minetest.register_node("firesafety:sign_" .. type .. "_" .. tostring(level), {
	        description = "Fire exit sign, " .. type .. ", light level " .. tostring(level),
	        light_source = level,
	        drawtype = "nodebox",
	        paramtype = "light",
	        paramtype2 = "facedir",
            groups = groups2,
	        sunlight_propagates = true,
	        
	        tiles = {
		        "fire_safety_black.png",
		        "fire_safety_black.png",
		        "fire_safety_black.png",
		        "fire_safety_black.png",
		        "fire_safety_black.png",
		        "fire_safety_sign_" .. type .. ".png",
	        },
	        
	        node_box = {
		        type = "fixed",
		        fixed = {
			        {-0.5, -0.1875, 0.375, 0.5, 0.1875, 0.5}, -- NodeBox1
		        }
	        },
	        
	        digilines = {
	            effector = {
	                action = function(pos, _, channel, msg)
	                    -- No channel checking; forcing users to manually bother with each and every sign in a massive building is almost a violation of human rights
	                
	                    if msg == "fire" then
                            minetest.set_node(pos, { name="firesafety:sign_" .. type .. "_17"})
                        elseif msg == "test" then
                            minetest.set_node(pos, { name="firesafety:sign_" .. type .. "_10"})
	                    elseif msg == "clear" then
                            minetest.set_node(pos, { name="firesafety:sign_" .. type .. "_2"})
	                    end
	                end 
	            }
	        }
        })
    end
    
    registerLightSign(type, 2)
    registerLightSign(type, 10)
    registerLightSign(type, 17)
    
    minetest.register_alias( "firesafety:sign_" .. type, "firesafety:sign_" .. type .. "_2") 
    
end

registerSign("forward")
registerSign("left")
registerSign("right")

minetest.register_craft({
	output = "firesafety:sign_forward 2",
	recipe = {
	{"dye:green","default:torch","dye:green"},
	{"default:torch","default:torch","default:torch"},
	{"default:wood 2","default:torch","default:wood 2"},
	}
})

--left
minetest.register_craft({
	output = "firesafety:sign_left 2",
	recipe = {
	{"dye:green","default:torch","default:wood 2"},
	{"default:torch","default:torch","default:torch"},
	{"dye:green","default:torch","default:wood 2"},
	}
})

--right
minetest.register_craft({
	output = "firesafety:sign_right 2",
	recipe = {
	{"default:wood 2","default:torch","dye:green"},
	{"default:torch","default:torch","default:torch"},
	{"default:wood 2","default:torch","dye:green"},
	}
})
